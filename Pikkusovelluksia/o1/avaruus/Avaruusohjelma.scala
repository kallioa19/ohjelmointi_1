package o1.avaruus
import o1.*

// Tämä koodi liittyy lukuun 2.7 ja esitellään siellä.

val avaruus = Avaruus(500)

val tausta = rectangle(avaruus.leveys, avaruus.korkeus, Black)
val maanKuva = circle(avaruus.maa.sade * 2, MediumBlue)
val kuunKuva = circle(avaruus.kuu.sade * 2, Beige)

// Kirjoita toimiva koodi kysymysmerkeillä merkittyihin kohtiin
object ikkuna extends View(avaruus, "Yksinkertainen näkymä avaruuteen"):

  def makePic =
    val tausta = rectangle(avaruus.leveys, avaruus.korkeus, Black)
    val kuuAvaruudessa = tausta.place(maanKuva, avaruus.maa.sijainti)
    kuuAvaruudessa.place(kuunKuva, avaruus.kuu.sijainti)

end ikkuna

@main def kaynnistaAvaruusohjelma() =
  ikkuna.start()
