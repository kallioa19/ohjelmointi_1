package o1.avaruus
import o1.*

// Tämä koodi liittyy lukuihin 2.6 ja 2.7 ja esitellään siellä.

class Taivaankappale(val nimi: String, val sade: Double, var sijainti: Pos):

  def halkaisija = this.sade * 2

  override def toString = this.nimi

end Taivaankappale



class Avaruus(koko: Int):

  val leveys = koko * 2
  val korkeus = koko

  val maa = Taivaankappale("Maa", 15.9, Pos(10,  this.korkeus / 2))
  val kuu = Taivaankappale("Kuu", 4.3,  Pos(971, this.korkeus / 2))

  override def toString = s"${this.leveys}x${this.korkeus} alue avaruudessa"

end Avaruus

