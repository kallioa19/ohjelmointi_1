package o1.palikka
import o1.*

// Tämä ohjelma liittyy lukuun 2.7 ja esitellään siellä.

val tausta = rectangle(500, 500, Black)

val palikka = Palikka(20, Pos(300, 50), Gray)

object nakyma extends View(palikka, "Palikkaohjelma"):

  def makePic =
    val palikanKuva = rectangle(palikka.koko, palikka.koko, palikka.vari)
    tausta.place(palikanKuva, palikka.sijainti)

end nakyma

@main def kaynnistaPalikkaohjelma() =
  nakyma.start()