package o1.taide
import o1.*

// Tämä koodi liittyy lukuun 3.1 ja esitellään siellä.

val teos = Taideprojekti(rectangle(600, 600, White))

object piirustusikkuna extends View(teos):

  def makePic = teos.kuva

  // Tapahtumankäsittelykoodi puuttuu.

end piirustusikkuna

@main def kaynnistaPiirustusohjelma() =
  piirustusikkuna.start()

