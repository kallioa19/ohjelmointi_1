package o1.laskuri
import o1.*

// Tämä koodi liittyy lukuun 3.1 ja esitellään siellä.

val tikityslaskuri = Laskuri(0)
val tausta = rectangle(500, 500, Black)

object nakyma extends View(tikityslaskuri, 50):

  def makePic =
    tausta.place(square(tikityslaskuri.arvo, White).clockwise(tikityslaskuri.arvo), Pos(250, 250))

  override def onTick() =
    tikityslaskuri.etene()

end nakyma


@main def kaynnistaTikitysohjelma() =
  nakyma.start()

