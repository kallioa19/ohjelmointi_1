package o1.laskuri
import o1.*

// Tämä koodi liittyy lukuun 3.1 ja esitellään siellä.

val klikkauslaskuri = Laskuri(5)
val sininenTausta = rectangle(500, 500, Blue)

object klikkausnakyma extends View(klikkauslaskuri):

  def makePic =
    sininenTausta.place(circle(klikkauslaskuri.arvo, White), Pos(100, 100))

  override def onClick(klikkauskohta: Pos) =
    klikkauslaskuri.etene()
    println("Klikkaus koordinaateissa " + klikkauskohta + "; " + klikkauslaskuri)

end klikkausnakyma


@main def kaynnistaKlikkausohjelma() =
  klikkausnakyma.start()

