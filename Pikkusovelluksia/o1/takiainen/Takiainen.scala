package o1.takiainen
import o1.*

// Tämä luokka liittyy lukuun 3.1 ja esitellään siellä.

class Takiainen:
  var sijainti = Pos(0, 0)  // tuoreimman säilyttäjä

val leveys = 800
val korkeus = 500
val tausta = rectangle(leveys, korkeus, White)
val takiaisenKuva = circle(40, YellowGreen)
