package o1.weather

import o1.*

val maa = rectangle(1500, 100, Black)

val paiva = rectangle(1500, 600, Blue)
val yo = rectangle(1500, 600, Black)
val ilta = rectangle(1500, 600, Red)

val puuRunko = rectangle(30, 250, SaddleBrown)
val latva = circle(100, Green)
val puu = puuRunko.place(latva, Center, TopCenter)

val toiminnot = toimintoja()

object maisema extends View(toiminnot, "Maisema"):
  
  def makePic =
    paiva.above(maa).place(puu, Pos(100, 500))
    
end maisema

@main def launchMaisema() =
  maisema.start()