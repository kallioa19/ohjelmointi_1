package o1.pyy
import o1.*

class Pyy:

  private var koko = 400
  private val peruskuva = Pic("ladybug1.png")
  

  def loppuKoitti = this.koko <= 0

  def pienene() =
    if this.koko > 0 then
      this.koko = this.koko - 1

  def kuvaksi = this.peruskuva.scaleTo(this.koko)

end Pyy