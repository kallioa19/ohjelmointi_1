package o1.pyy
import o1.*

val pyy = Pyy()

object nakyma extends View(pyy, 50):

  val valkoinenTausta = rectangle(600, 600, White)
  val maailmanlopunKuva = rectangle(600, 600, Black)

  def makePic =
    if pyy.loppuKoitti then
      maailmanlopunKuva
    else
      pyy.kuvaksi.onto(valkoinenTausta)

  override def onTick() =
    pyy.pienene()

  override def isDone = pyy.loppuKoitti

end nakyma


@main def kaynnista() =
  nakyma.start()
