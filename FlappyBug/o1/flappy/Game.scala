package o1.flappy

import o1.*

class Game:

  val bug = Bug(Pos(100.0, 40.0))

  val obstacle = Obstacle(70)

  def activateBug() =
    bug.flap(15)

  def timePasses() =
    bug.fall()
    obstacle.approach()
    
  def isLost =
    this.obstacle.touches(this.bug) || !this.bug.isInBounds

end Game