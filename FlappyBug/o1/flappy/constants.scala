package o1.flappy

val ObstacleSpeed = 10
val FallingSpeed = 2
val BugRadius = 15
val ViewWidth = 1000
val ViewHeight = 400
val GroundDepth = 50
val BackgroundSpeed = 2