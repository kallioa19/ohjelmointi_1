package o1.flappy

import o1.*
import scala.math.*
import scala.util.Random

class Obstacle(val radius: Int):

  private var currentPos = Pos(ViewWidth + this.radius + Random.nextInt(500),Random.nextInt(400))
  def pos = this.currentPos

  private def randomLaunchPosition() =
    val launchX = ViewWidth + this.radius + Random.nextInt(500)
    val launchY = Random.nextInt(400)
    Pos(launchX, launchY)

  def approach() =
    if this.isActive then
      this.currentPos = this.currentPos.addX(-ObstacleSpeed)
    else
      this.currentPos = randomLaunchPosition()
  def touches(bug: Bug) =
    sqrt(pow(this.pos.x - bug.pos.x, 2) + pow(this.pos.y - bug.pos.y, 2)) < this.radius + bug.radius

  def isActive =
    this.currentPos.x >= -this.radius

  override def toString = "center at " + this.pos + ", radius " + this.radius

end Obstacle