package o1.flappy

import o1.*

class Bug(private var currentPos: Pos):

  val radius = BugRadius

  def pos = currentPos

  private var yVelocity = 0.0

  def move(muutos: Double) =
    currentPos = currentPos.addY(muutos)
    currentPos = currentPos.clampY(0.0, 350.0)

  def flap(nopeus: Double) =
    yVelocity = -nopeus

  def fall() =
    if pos.y < 350 then
      yVelocity += 2
    move(yVelocity)

  def isInBounds =
    this.currentPos.y > 0 && this.currentPos.y < 350

  override def toString =
    s"center at ${this.pos}, radius ${this.radius}"

end Bug