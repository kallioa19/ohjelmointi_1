package o1.flappy

import o1.*

val sky        = rectangle(ViewWidth, ViewHeight,  LightBlue)
val ground     = rectangle(ViewWidth, GroundDepth, SandyBrown)
val trunk      = rectangle(30, 250, SaddleBrown)
val foliage    = circle(200, ForestGreen)
val tree       = trunk.onto(foliage, TopCenter, Center)
val rootedTree = tree.onto(ground, BottomCenter, Pos(ViewWidth / 2, 30))
val scenery    = sky.place(rootedTree, BottomLeft, BottomLeft)

val bugPic = Pic("ladybug.png")

def rockPic(obstacle: Obstacle) = circle(obstacle.radius * 2, Black)

val peli = Game()

//val este = rockPic(peli.obstacle)
val este = Pic("obstacle.png")

object flappyView extends View(peli, "FlappyBug"):

  var background = scenery

  def makePic =
    background.place(bugPic, peli.bug.pos).place(este, peli.obstacle.pos)

  override def onKeyDown(painettu: Key) =
    if painettu == Key.Space then
      peli.activateBug()

  override def onTick() =
    peli.timePasses()
    this.background = this.background.shiftLeft(BackgroundSpeed)

  override def isDone =
    peli.isLost

end flappyView

@main def launchFlappy() =
  flappyView.start()