package o1.misc
import o1.*

def describe(pic: Pic) =
  if pic.height > pic.width then
    "portrait"
  else
    "landscape"