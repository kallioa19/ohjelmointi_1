package o1.yksittaisia

// Alla on lukuun 2.1 liittyvä esimerkki. Sen sisäistä toimintaa ei tarvitse ymmärtää.
// Kaikkea seuraavasta koodista ei ole kirjoitettu aloittelijaystävälliseen tyyliin.

import o1.play
import o1.util.mapFromID
import scala.util.Try

object radio:
  private val pikavalinnat = Vector(87900, 94000, 94900, 98500)
  private val asemat: Map[Int, Station] = Seq(
    Station(98500, "Radio Helsinki", // with apologies to Claire Boucher:
            "[39]<<" + "CC  CC  CC  CC  " * 2 + "&[33]<<<" + "CC  CC  CC  CC  " * 2 + "&P:<<                  D   D   D   D &[39]<" + "cc>c<gccc>c<|cc>c<gc>c<gg" * 2 + "/150"),
    Station(94000, "Radio Suomi", // with apologies to Fucked Up:
            "[41]>g-g-gg-g-g#-g---f-f--gfd#-d#-----c-c-cc<hb-----hb>ddd--dd#-d#---/180"),
    Station(87900, "YLE 1", // with apologies to Chairlift:
            "[17]edc<a>c-d<a-------hageg-ad-c<a----/150"),
    Station(94900, "Radio Rock", // with apologies to the old Ludwig Van:
            "[2](<G>G)(<G>G)(<G>G)(<Eb>Eb)---------- (<F>F)(<F>F)(<F>F)(<D>D)-------------- &[2]<<(<G>G)(<G>G)(<G>G)(<Eb>Eb)-----------(<F>F)(<F>F)(<F>F)(<D>D)---------------/180"),
    Station(106200, "Radio Nova", // with apologies to Fiona Apple:
            "[33]<<c# c#e# e#d de# e#c# c#e# e#c#&[46]<   g# g#   g# g#   g# g#>[113]  >E---"),
    Station(91900, "YleX", // with apologies to Heikki Kuula:
            "[110]>E-E-EDC-<A-G->E-E-EDC-<A->C-<A-G-    >E-E-EGE-D-C-E-E-EGE-D-C-[112]e-d-/150")
  ).mapFromID( _.kHz )

  val pykalaKHz = 100
  private var taajuus = pikavalinnat(1)

  def taajuusKHz_=(uusiTaajuus: Int) =
    this.taajuus = uusiTaajuus
    Try(this.biisiNyt.foreach(o1.play))

  def taajuusKHz = this.taajuus

  def virita(pykalat: Int) =
    this.taajuusKHz += pykalaKHz * pykalat
    this.kuvaus

  def valitse(monesko: Int) =
    for valittu <- this.pikavalinnat.lift(monesko - 1) do
      this.taajuusKHz = valittu
    this.kuvaus

  private def asemaNyt = this.asemat.get(this.taajuusKHz)

  private def biisiNyt = this.asemaNyt.map( _.soitossa )

  private def kuvaus = 
    val megaHz = this.taajuusKHz / 1000.0
    val asema = this.asemaNyt.map( _.nimi ).getOrElse("vain kohinaa")
    f"$megaHz%.1f MHz: $asema"

  private class Station(val kHz: Int, val nimi: String, val soitossa: String)

end radio

