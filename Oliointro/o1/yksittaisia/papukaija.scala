// ALLA ON LUKUUN 2.1 LIITTYVÄ ESIMERKKI. SEN SISÄISTÄ TOIMINTAA EI TARVITSE YMMÄRTÄÄ.
// KAIKKEA SEURAAVASTA KOODISTA EI MYÖSKÄÄN OLE KIRJOITETTU ALOITTELIJAYSTÄVÄLLISEEN TYYLIIN.
package o1.yksittaisia

object papukaija:

  private var repertuaari = List("Huh hellettä ja rommia pullo", "Polly tahtoo keksin")

  def vastaa(kuultuLause: String) =

    def siisti(lause: String)   = lause.replaceAll(raw"[^\w åäöÅÄÖ]+", "")

    def sanoiksi(lause: String) = siisti(lause).toLowerCase.split(" ").filter( _.length > 2 )

    val kuullutSanat = sanoiksi(kuultuLause)

    def samantapainen(sana: String, toinen: String) = o1.util.editDistance(sana, toinen, 1) <= 1

    def sisaltaaKuulluntapaisenSanan(lause: String) =
      sanoiksi(lause).exists( tuttuSana => kuullutSanat.exists( samantapainen(_, tuttuSana)) )

    val valmisRepliikki = this.repertuaari.find(sisaltaaKuulluntapaisenSanan)
    val vastaus = valmisRepliikki.getOrElse(siisti(kuultuLause).takeWhile( _ != ' ' ))
    if vastaus.nonEmpty then vastaus + "!" else ""

  end vastaa

  def opiLause(uusiLause: String) =
    this.repertuaari = uusiLause :: this.repertuaari

end papukaija

