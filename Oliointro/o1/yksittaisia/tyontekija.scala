// Seuraavasta alkumäärittelystä ei tarvitse tässä vaiheessa välittää mitään.
package o1.yksittaisia

// Tämä esimerkki käsitellään luvussa 2.2.

object tyontekija:
  var nimi = "Matti Mikälienen"
  val syntynyt = 1965
  var kkpalkka = 5000.0
  var tyoaika = 1.0

  def ikaVuonna(vuosi: Int) = vuosi - this.syntynyt

  def kuukausikulut(kulukerroin: Double) =
    this.kkpalkka * this.tyoaika * kulukerroin

  def korotaPalkkaa(kerroin: Double) =
    this.kkpalkka = this.kkpalkka * kerroin

  def kuvaus =
    this.nimi + " (s. " + this.syntynyt + "), palkka " + this.tyoaika + " * " + this.kkpalkka + " euroa"

end tyontekija
