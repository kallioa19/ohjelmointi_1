package o1.luokkia

// Tämä luokka liittyy lukuun 2.6 ja esitellään siellä.

class Asiakas(val nimi: String, val asiakasnumero: Int, val email: String, val osoite: String):
  
  //def kuvaus = "#" + this.asiakasnumero + " " + this.nimi + " <" + this.email + ">"

  override def toString = "#" + this.asiakasnumero + " " + this.nimi + " <" + this.email + ">"

end Asiakas
