package o1.odds

// This program is developed in Chapters 2.7 and 3.4.
// It creates a single Odds object and uses some of its methods.

import scala.io.StdIn.*

object OddsTest1 extends App:

  def requestOdds() =
    val firstInput = readInt()
    val secondInput = readInt()
    Odds(firstInput, secondInput)

  println("Please enter the odds of an event as two integers on separate lines.")
  println("For instance, to enter the odds 5/1 (one in six chance of happening), write 5 and 1 on separate lines.")

  val odds = requestOdds()

  println(s"The odds you entered are:")
  println(s"In fractional format: ${this.odds.fractional}")
  println(s"In decimal format: ${this.odds.decimal}")
  println(s"In moneyline format: ${this.odds.moneyline}")
  println(s"Event probability: ${this.odds.probability}")
  println(s"Reverse odds: ${this.odds.not}")
  println(s"Odds of happening twice: ${this.odds.both(this.odds)}")

  println("Please enter the size of a bet:")
  val thirdInput = readDouble()
  println(s"If successful, the bettor would claim ${this.odds.winnings(this.thirdInput)}")

  println("Please enter the odds of a second event as two integers on separate lines.")
  val odds2 = requestOdds()

  println(s"The odds of both events happening are: ${this.odds.both(this.odds2)}")
  println(s"The odds of one or both happening are: ${this.odds.either(this.odds2)}")

end OddsTest1