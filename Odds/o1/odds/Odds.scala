package o1.odds

import scala.math.*


// This class is developed gradually between Chapters 2.4 and 3.4.

val o = Odds(4, 2)
val p = Odds(6, 5)


class Odds(val wont: Int, val will: Int):

  def probability =
    1.0 * this.will / (this.wont + this.will)

  def fractional =
    s"${this.wont}/${this.will}"

  def decimal =
    pow(this.probability, -1)

  def winnings(panos: Double) =
    panos * this.decimal

  def not =
    Odds(this.will, this.wont)

  override def toString =
    fractional

  def both(that: Odds) =

    val a1 = this.wont
    val a2 = this.will
    val b1 = that.wont
    val b2 = that.will

    Odds((a1*b1+a1*b2+a2*b1),(a2*b2))

  def either(that: Odds) =

    val a1 = this.wont
    val a2 = this.will
    val b1 = that.wont
    val b2 = that.will

    Odds((a1*b1),(a1*b2+a2*b1+a2*b2))

  def isLikely =
    this.probability > 0.5

  def isLikelierThan(that: Odds) =
    this.probability > that.probability

  def moneyline =
    if this.probability <= 0.50 then
      100 * this.wont / this.will
    else
      -100 * this.will / this.wont

end Odds

def kokeilu(x: Int): Int =
  if x < 20 then
    1 + kokeilu(x + 1)
  else
    0

def listsum(joukko: List[Int]): Int =
  val l = joukko.length
  def sumint(n: Int): Int =
    if n < l then
      joukko(n) + sumint(n+1)
    else
      0
  sumint(0)
end listsum

def emptornempt(joukko: List[Int]): String = joukko match
  case Nil => "empt"
  case _ => "nempt"


def recsum(viimeinenLuku: Int) =
  viimeinenLuku * (viimeinenLuku + 1) / 2

var tn1: Double = 1.0/27
var tn2: Double = 1.0/10
def tn = tn1 * tn2