package o1.carsim
import o1.Pos


class Car(val fuelConsumption: Double, val tankSize: Double, initialFuel: Double, initialLocation: Pos):

  def location: Pos = ???                                               // TODO: replace ??? with a working implementation

  def fuel(toBeAdded: Double): Double = ???                             // TODO: replace ??? with a working implementation

  def fuel(): Double = ???                                              // TODO: replace ??? with a working implementation

  def fuelRatio: Double = ???                                           // TODO: replace ??? with a working implementation

  def metersDriven: Double = ???                                        // TODO: replace ??? with a working implementation

  def fuelRange: Double = ???                                           // TODO: replace ??? with a working implementation

  def drive(destination: Pos, metersToDestination: Double): Unit = ???  // TODO: replace ??? with a working implementation

end Car

