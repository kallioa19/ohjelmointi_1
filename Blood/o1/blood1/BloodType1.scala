package o1.blood1

class BloodType(val abo: String, val rhesus: Boolean):

  def rhe =
    if rhesus then
      "+"
    else
      "-"

  override def toString =
    abo + rhe

  def hasSafeABOFor(recipient: BloodType) =
    if this.abo == "A" && (recipient.abo == "A" || recipient.abo == "AB") then
      true
    else if this.abo == "B" && (recipient.abo == "B" || recipient.abo == "AB") then
      true
    else if this.abo == "AB" && recipient.abo == "AB" then
      true
    else if this.abo == "O" then
      true
    else
      false



  def hasSafeRhesusFor(recipient: BloodType) =
    if !this.rhesus then
      true
    else if recipient.rhesus then
      true
    else
      false

  def canDonateTo(recipient: BloodType) =
    this.hasSafeABOFor(recipient) && this.hasSafeRhesusFor(recipient)

  def canReceiveFrom(donor: BloodType) =
    donor.hasSafeABOFor(this) && donor.hasSafeRhesusFor(this)

end BloodType