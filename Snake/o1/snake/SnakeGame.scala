package o1.snake

import o1.*

// Represents games of Snake. A SnakeGame object is mutable: it tracks the
// position and heading of a snake as well as the position of a food item that
// is available for the snake to eat next.
class SnakeGame(initialPos: GridPos, initialHeading: CompassDir):

  private var segments = Vector(initialPos) // container: the locations of every snake segment, in order from head to tail
  var snakeHeading = initialHeading         // most-recent holder (the direction most recently set for the snake)
  var nextFood = randomLocationOnGrid()     // most-recent holder (a changing random location for the next available food item)

  def snakeSegments = this.segments

  def isOver =
    val head = this.segments.head
    val tail = this.segments.tail
    val validCoords = 1 until SizeInSquares
    val collidedWithWall = !validCoords.contains(head.x) || !validCoords.contains(head.y)
    val collidedWithItself = tail.contains(head)
    collidedWithWall || collidedWithItself

  // This gets repeatedly called as the game progresses. It advances the snake by one
  // square in its current heading. In case the snake finds food, it grows by one segment,
  // the current nextFood vanishes, and new food is placed in a random location.
  def advance() =
    if segments.head.neighbor(snakeHeading) == nextFood then
      segments = segments.head.neighbor(snakeHeading) +: segments
    else
      segments = segments.head.neighbor(snakeHeading) +: segments.init

    if this.segments.contains(this.nextFood) then
      this.nextFood = randomLocationOnGrid()

end SnakeGame

