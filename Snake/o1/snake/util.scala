package o1.snake

import o1.GridPos
import scala.util.Random

// The height and width of the game area, measured in squares that can hold
// a single segment of a snake.
val SizeInSquares = 40

// Randomly returns the position of a single square on the grid, excluding the
// very edges (where no food can appear and which kill the snake if it enters).
def randomLocationOnGrid() =
  GridPos(Random.nextInt(SizeInSquares - 2) + 1,
          Random.nextInt(SizeInSquares - 2) + 1)

