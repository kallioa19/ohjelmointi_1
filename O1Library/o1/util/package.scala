/** The package `o1.util` contains miscellaneous tools that are used internally by some of the
  * given programs in O1 for added convenience.
  *
  * **NOTE TO STUDENTS: In this course, you don’t need to understand how this package works
  * or can be used.**
  *
  * This documentation lists only some of the tools in the package. The listed tools are largely
  * a mix of:
  *
  *  - functions for simple I/O from files and URLs;
  *  - aliases for easy access (via `import o1.util.*`)
  *    to some of the tools from `scala.util`; and
  *  - implicit classes that add a few convenience methods
  *    to selected types from the Scala API.  */
package o1.util

import scala.annotation.targetName
import scala.languageFeature.higherKinds
import scala.quoted.*


///////////////////////////////////
///// ALIASES FOR CONVENIENCE
///////////////////////////////////

/** An alias for convenient use of `scala.util.Try` via `import o1.util.*`. */
type Try[T] = scala.util.Try[T]
/** An alias for convenient use of `scala.util.Try` via `import o1.util.*`. */
val Try = scala.util.Try


/** An alias for convenient use of `scala.util.Try` via `import o1.util.*`. */
type Success[T] = scala.util.Success[T]
/** An alias for convenient use of `scala.util.Try` via `import o1.util.*`. */
val Success = scala.util.Success


/** An alias for convenient use of `scala.util.Try` via `import o1.util.*`. */
type Failure[T] = scala.util.Failure[T]
/** An alias for convenient use of `scala.util.Try` via `import o1.util.*`. */
val Failure = scala.util.Failure


/** An alias for convenient use of `scala.util.Random` via `import o1.util.*`. */
type Random = scala.util.Random
/** An alias for convenient use of `scala.util.Random` via `import o1.util.*`. */
val Random = scala.util.Random


/** An alias for convenient use of `scala.math.Ordering.Double.TotalOrdering` via `import o1.util.*`. */
val DoubleOrdering = scala.math.Ordering.Double.TotalOrdering


/** An alias for convenient use of `java.net.URL` via `import o1.util.*`. */
type URL = java.net.URL

/** An alias for convenient use of `java.nio.file.Path` via `import o1.util.*`. */
type Path = java.nio.file.Path

/** An alias for convenient use of `scala.io.Source` via `import o1.util.*`. */
type Source = scala.io.Source
given CanEqual[Source, Source] = CanEqual.derived
/** An alias for convenient use of `scala.io.Source` via `import o1.util.*`. */
val Source = scala.io.Source


/** An alias for convenient access to `java.lang.System.getProperty("user.dir")` via `import o1.util.*`.*/
def workingDir = System.getProperty("user.dir")


/////////////////////////////////////////////
// CONVENIENCE MACROS
/////////////////////////////////////////////

/** A macro that returns a string describing the structure (abstract syntax tree) of the given Scala expression. */
inline def ast(inline expr: Any): String = ${ exprToAST('expr) }

private def exprToAST(expr: Expr[Any])(using Quotes): Expr[String] = Expr(expr.show)


/** A macro that prints out the given expressions. Literals are printed as they are, other expressions
  * as `"expr=value"`. Potentially useful for debugging. */
inline def report(inline exprs: Any*): Unit = ${exprsToReport('exprs)}  // Directly based on: https://github.com/softwaremill/scala3-macro-debug

private def exprsToReport(exprs: Expr[Seq[Any]])(using quotes: Quotes): Expr[Unit] =
  import quotes.reflect.*
  def cleanReplIds(id: String) = if id.startsWith("repl.") then id.drop(id.lastIndexOf(".") + 1) else id
  def exprWithValue(expr: Expr[?]): Expr[String] = '{ ${ Expr(cleanReplIds(expr.show)) } + "=" + $expr }
  def toReportPiece(expr: Expr[?]): Expr[String] = expr.asTerm match
    case Literal(const: Constant) => Expr(const.value.toString)
    case nonLiteral               => exprWithValue(expr)
  val pieceExprs: Seq[Expr[String]] = exprs match
    case Varargs(exprs) => exprs.map(toReportPiece)
    case singleExpr     => List(exprWithValue(singleExpr))
  val reportExpr = pieceExprs.reduceLeftOption( (expr1, expr2) => '{ $expr1 + ", " + $expr2 })
  '{ println(${ reportExpr getOrElse '{ "" } }) }



////////////////////////
///// TAGGED TYPES (no longer used internally)
////////////////////////

// based on http://etorreborre.blogspot.fi/2011/11/practical-uses-for-unboxed-tagged-types.html
private[o1] type Tagged[TagType] = { type Tag = TagType }
@targetName("Tag")
private[o1] type @@[BaseType, TagType] = BaseType & Tagged[TagType]




////////////////////////////////
///// MISCELLANEOUS
////////////////////////////////


/** A label that can be used as an empty method body or other “no-op” block. */
val DoNothing: Unit = ()


/** Performs a given computation and checks to see if it crashes with a `NotImplementedError`.
  * Returns the result of the computation in an `Option`; returns `None` only in the
  * computation wasn’t implemented. Any other exception is thrown.
  * @param  computation  a computation that may or may not be implemented
  * @tparam Result       the type of the given computation  */
def ifImplemented[Result](computation: =>Result): Option[Result] =
  Try(computation) match
    case Success(result)                       => Some(result)
    case Failure(missing: NotImplementedError) => None
    case Failure(miscProblem)                  => throw miscProblem


/** Performs a given computation and determines whether it crashes with a `NotImplementedError`.
  * Returns `true` if it did and `false` otherwise.
  * @param  computation  a computation that may or may not be implemented
  * @tparam Result       the type of the given computation  */
def isImplemented[Result](computation: =>Result): Boolean =
  ifImplemented(computation).isDefined


private[o1] def editDistance(text1: String, text2: String, threshold: Int): Int =
  if text1.isEmpty                 then if text2.length <= threshold then text2.length else Int.MaxValue
  else if text2.isEmpty            then if text1.length <= threshold then text1.length else Int.MaxValue
  else if text1.head == text2.head then editDistance(text1.tail, text2.tail, threshold)
  else if threshold == 0           then Int.MaxValue
  else
    val deletion     = editDistance(text1.tail, text2,      threshold - 1)
    val insertion    = editDistance(text1,      text2.tail, threshold - 1)
    val substitution = editDistance(text1.tail, text2.tail, threshold - 1)
    val shortest = Seq(deletion, insertion, substitution).min
    if shortest == Int.MaxValue then Int.MaxValue else shortest + 1


private[o1] def repeatEvery(interval: Int)(timedBlock: => Unit): Ticker =
  val ticker = Ticker(interval, Ticker.TickEvery(interval))(timedBlock)
  ticker.start()
  ticker


private[o1] object Program: // not entirely reliable, but good enough for some purposes

  lazy val isRunningInScalaREPL: Boolean =
    Try(Class.forName("dotty.tools.repl.ReplDriver")).isSuccess

  lazy val isRunningInGTK: Boolean =
    javax.swing.UIManager.getSystemLookAndFeelClassName.endsWith("GTKLookAndFeel")

end Program


private[o1] final class FirstTimeEffect(effect: => Unit):
  private lazy val storedEffect = effect
  def apply(): Unit = storedEffect

private[o1] object firstTimeOnly:
  def apply(effect: => Unit): FirstTimeEffect = FirstTimeEffect(effect)

//noinspection UnitMethodIsParameterless
private[o1] inline def pass: Unit = ()


/////////////////////////////////////
// INTERNAL: SMCL INITIALIZATION
/////////////////////////////////////

private val smclInitialization = DoneStatus()

private final class DoneStatus:
  private var hasNotBeenDone = true

  def isNotDone: Boolean = hasNotBeenDone

  def setDone(): Unit =
    if hasNotBeenDone then
      synchronized( if hasNotBeenDone then hasNotBeenDone = false )

end DoneStatus


private[o1] def smclInit(): Unit =
  import smcl.*
  import settings.jvmawt.*
  if smclInitialization.isNotDone then
    synchronized {
      if smclInitialization.isNotDone then
        infrastructure.jvmawt.Initializer()
        settings.DefaultBackgroundColor = colors.rgb.Transparent // Affects the bg color for bitmap rotations
        AffineTransformationInterpolationMethod = Bicubic        // Affects affine transformations incl. scaling, rotations, and flips
        // smcl.settings.jvmawt.DrawingIsAntialiased = false
        smclInitialization.setDone()
    }

private val _ = smclInit()

