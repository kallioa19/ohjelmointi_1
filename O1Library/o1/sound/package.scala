/** Please see one of the two subpackages:
  *
  *  - [[o1.sound.midi]] for MIDI sound described in terms of notes,
  *    instruments, and other directives
  *
  *  - [[o1.sound.sampled]] for working with recorded sound samples. */
package o1.sound

/** See [[o1.sound.midi]] for MIDI sound and [[o1.sound.sampled]] for recorded sound samples. */
object O_o   // Apparently Scaladoc doesn’t (2/2022) generate a top-level page for a package unless it directly contains stuff.
