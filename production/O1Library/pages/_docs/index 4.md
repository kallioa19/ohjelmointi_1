
# O1Library

<!-- NOTE: This is redundant with api_root_page.md, so duplicate any edits there. -->

For an overview, see [[Introduction to O1Library|start]].

For Scaladocs of O1Library’s contents, see the [[O1Library API documentation|o1]]. 

A summary of O1Library’s MIDI notation is on a [[separate page|play]].

There’s also a [[Credits|credits]] page with author info.

