
<!-- NOTE: This is redundant with docs/index.md, so duplicate any edits there. -->

For an overview, see [Introduction to O1Library](docs/start.html).

For Scaladocs of O1Library’s contents (API), see below on this page and follow the links.

A summary of O1Library’s MIDI notation is on a [separate page](_docs/play.html).

There’s also a [Credits](_docs/credits.html) page with author info.

