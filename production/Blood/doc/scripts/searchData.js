pages = [{"l":"index.html#","e":false,"i":"","n":"Blood Docs","t":"Blood Docs","d":"","k":"static"},
{"l":"o1/blood1.html#","e":false,"i":"","n":"o1.blood1","t":"o1.blood1","d":"","k":"package"},
{"l":"o1/blood1/BloodType.html#","e":false,"i":"","n":"BloodType","t":"BloodType(val abo: String, val rhesus: Boolean)","d":"o1.blood1","k":"class"},
{"l":"o1/blood1/BloodType.html#abo-0","e":false,"i":"","n":"abo","t":"abo: String","d":"o1.blood1.BloodType","k":"val"},
{"l":"o1/blood1/BloodType.html#canDonateTo-6b7","e":false,"i":"","n":"canDonateTo","t":"canDonateTo(recipient: BloodType): Boolean","d":"o1.blood1.BloodType","k":"def"},
{"l":"o1/blood1/BloodType.html#canReceiveFrom-6b7","e":false,"i":"","n":"canReceiveFrom","t":"canReceiveFrom(donor: BloodType): Boolean","d":"o1.blood1.BloodType","k":"def"},
{"l":"o1/blood1/BloodType.html#hasSafeABOFor-6b7","e":false,"i":"","n":"hasSafeABOFor","t":"hasSafeABOFor(recipient: BloodType): Boolean","d":"o1.blood1.BloodType","k":"def"},
{"l":"o1/blood1/BloodType.html#hasSafeRhesusFor-6b7","e":false,"i":"","n":"hasSafeRhesusFor","t":"hasSafeRhesusFor(recipient: BloodType): Boolean","d":"o1.blood1.BloodType","k":"def"},
{"l":"o1/blood1/BloodType.html#rhesus-0","e":false,"i":"","n":"rhesus","t":"rhesus: Boolean","d":"o1.blood1.BloodType","k":"val"}];